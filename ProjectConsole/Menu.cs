﻿using ConsoleTables;
using ProjectConsole.Services;
using Projects.Common.DTOmodels;
using System;
using System.Threading.Tasks;

namespace ProjectConsole
{
    public class Menu
    {
        private readonly UserService _userService = new UserService();
        private readonly TaskService _taskService = new TaskService();
        private readonly ProjectService _projectService = new ProjectService();
        private readonly TeamService _teamService = new TeamService();

        private static void Error(string error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Error! {error}");
            Console.ResetColor();
        }

        public async Task ShowMenu()
        {
            int Key = 0;
            Console.ResetColor();
            while (true)
            {
                Console.WriteLine(Helper.MainMenu);
                try
                {
                    Key = int.Parse(Console.ReadLine());
                    if (Key > 9 || Key < 1)
                    {
                        throw new ArgumentException("Wrong entry!");
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("No such item! Please select menu item from 1 to 9!");
                    Console.ResetColor();
                }

                switch (Key)
                {
                    case 1:
                        await ShowUserTasksPerProject();
                        break;
                    case 2:
                        await ShowListOfTasksForUser();
                        break;
                    case 3:
                        await ShowListOfTasksFinished();
                        break;
                    case 4:
                        await ShowTeams();
                        break;
                    case 5:
                        await ShowUsers();
                        break;
                    case 6:
                        await ShowUserLastProject();
                        break;
                    case 7:
                        await ShowProjects();
                        break;
                    case 8:
                        await ShowCRUDMenu();
                        break;
                    case 9: return;

                }
            }
        }

        private async Task ShowCRUDMenu()
        {
            int Key = 0;
            Console.ResetColor();
            while (true)
            {
                Console.WriteLine(Helper.CrudMenu);
                try
                {
                    Key = int.Parse(Console.ReadLine());
                    if (Key > 5 || Key < 1)
                    {
                        throw new ArgumentException("Wrong entry!");
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("No such item! Please select menu item from 1 to 5!");
                    Console.ResetColor();
                }

                switch (Key)
                {
                    case 1:
                        await ShowCrudMenuOperations(new UserDTO());
                        break;
                    case 2:
                        await ShowCrudMenuOperations(new ProjectDTO());
                        break;
                    case 3:
                        await ShowCrudMenuOperations(new TeamDTO());
                        break;
                    case 4:
                        await ShowCrudMenuOperations(new TaskDTO());
                        break;
                    case 5: return;
                }
            }
        }

        private async Task ShowCrudMenuOperations<T>(T item)
        {
            int Key = 0;
            Console.ResetColor();
            while (true)
            {
                Console.WriteLine(Helper.CrudOperations);
                try
                {
                    Key = int.Parse(Console.ReadLine());
                    if (Key > 6 || Key < 1)
                    {
                        throw new ArgumentException("Wrong entry!");
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("No such item! Please select menu item from 1 to 5!");
                    Console.ResetColor();
                }

                switch (Key)
                {
                    case 1:
                        await Add(item);
                        break;
                    case 2:
                        await Delete(item);
                        break;
                    case 3:
                        await Update(item);
                        break;
                    case 4:
                        await Find(item);
                        break;
                    case 5:
                        await ShowAllAsync(item);
                        break;
                    case 6: return;
                }
            }
        }

        private async Task ShowAllAsync<T>(T item)
        {
            switch (item)
            {
                case UserDTO:
                    await ShowAllUsersAsync();

                    break;
                case ProjectDTO:
                    await ShowAllProjectsAsync();
                    break;
                case TeamDTO:
                    await ShowAllTeamsAsync();
                    break;
                case TaskDTO:
                    await ShowAllTasksAsync();
                    break;
                default: return;
            }


        }

        private async Task ShowAllTasksAsync()
        {
            var tasks = await _taskService.ShowTasksAsync();
            var table = new ConsoleTable("id", "Name", "Description", "PerformerId", "FinishedAt", "ProjectId", "State", "CreatedAt");

            ShowBorder(" List of all tasks ");

            foreach (var task in tasks)
            {
                table.AddRow(task.Id, task.Name, Helper.CutString(task.Description,23), task.PerformerId, task.FinishedAt, task.ProjectId, task.State, task.CreatedAt);
            }
            table.Write();

        }

        private async Task ShowAllTeamsAsync()
        {
            var teams = await _teamService.ShowTeamsAsync();
            var table = new ConsoleTable("id", "Name", "CreatedAt");

            ShowBorder(" List of all teams ");

            foreach (var team in teams)
            {
                table.AddRow(team.Id, team.Name, team.CreatedAt);
            }
            table.Write();
        }

        private async Task ShowAllProjectsAsync()
        {
            var projects = await _projectService.ShowProjectsAsync();
            var table = new ConsoleTable("id", "Name", "Description", "AuthorId", "TeamId", "CreatedAt");

            ShowBorder(" List of all projects ");

            foreach (var project in projects)
            {
                table.AddRow(project.Id, Helper.CutString(project.Name, 25), Helper.CutString(project.Description, 25), project.AuthorId, project.TeamId, project.CreatedAt);
            }
            table.Write();
        }

        private async Task ShowAllUsersAsync()
        {
            var users = await _userService.ShowUsersAsync();
            var table = new ConsoleTable("id", "TeamId", "FirstName", "LastName", "Email", "RegisteredAt", "BirthDay");

            ShowBorder(" List of all users ");

            foreach (var user in users)
            {
                table.AddRow(user.Id, user.TeamId, user.FirstName, user.LastName, user.Email, user.RegisteredAt, user.BirthDay);
            }
            table.Write();

        }

        private async Task Find<T>(T item)
        {
            switch (item)
            {
                case UserDTO:
                    await _userService.FindUserById();
                    break;
                case ProjectDTO:
                    await _projectService.AddProjectAsync();
                    break;
                case TaskDTO:
                    await _taskService.AddTaskAsync();
                    break;
                case TeamDTO:
                    await _teamService.AddTeamAsync();
                    break;
            }
        }

        private Task Update<T>(T item)
        {
            throw new NotImplementedException();
        }

        private async Task Delete<T>(T item)
        {
            switch (item)
            {
                case UserDTO:
                    await _userService.DeleteUserAsync();
                    break;
                case ProjectDTO:
                    await _projectService.DeleteProjectAsync();
                    break;
                case TaskDTO:
                    await _taskService.DeleteTaskAsync();
                    break;
                case TeamDTO:
                    await _teamService.DeleteTeamAsync();
                    break;
            }
        }

        private async Task Add<T>(T item)
        {
            switch (item)
            {
                case UserDTO:
                    await _userService.AdduserAsync();
                    break;
                case ProjectDTO:
                    await _projectService.AddProjectAsync();
                    break;
                case TaskDTO:
                    await _taskService.AddTaskAsync();
                    break;
                case TeamDTO:
                    await _teamService.AddTeamAsync();
                    break;
            }
        }


        private async Task ShowUsers()
        {
            Console.Clear();
            var users = await RequestService.Task5();

            ShowBorder("Project with longest task by description + shortest task by name and count of performers");
            foreach (var user in users)
            {
                Console.WriteLine($"User id = {user.User.Id} name => ( {user.User.FirstName} ) last name =>( {user.User.LastName} )");
                Console.WriteLine("User tasks: ");
                foreach (var task in user.Tasks)
                {
                    Console.WriteLine($"Task id = {task.Id} name {task.Name}");
                }
                Console.WriteLine();
            }

        }

        private static async Task ShowProjects()
        {
            Console.Clear();
            var projects = await RequestService.Task7();

            ShowBorder("Project with longest task by description + shortest task by name and count of performers");
            foreach (var project in projects)
            {
                Console.WriteLine($"Project id =>( {project.Project.Id} ) name =>( {project.Project.Name} ) \n" +
                    $"longest task by description is ( {project.LongestTaskDescription?.Description ?? string.Empty} ) " +
                    $"\nShortest task by name is ( {project.ShortestTaskByName?.Name ?? string.Empty} )" +
                    $"\nPerformers number => [ {project?.UsersCount ?? 0} ]");
                Console.WriteLine();
            }
        }

        private static async Task ShowUserLastProject()
        {
            int ID;
            while (true)
            {
                Console.Write("Please enter user Id: ");
                try
                {
                    ID = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            var user = await RequestService.Task6(ID);
            if (user == null)
            {
                ShowBorder($"There is no user with ID = [{ID}] with projects and tasks... ");
                return;
            }
            ShowBorder($"User id = {user.User.Id} name =>( {user.User.FirstName} ) last name => ( {user.User.LastName} )");
            Console.WriteLine($"Last project is => ( {user.LastProject.Name} ) created at {user.LastProject.CreatedAt}");
            Console.WriteLine($"Longest task is => ( {user.LongestTask.Name} ) created at {user.LongestTask.CreatedAt} and finished at {user.LongestTask.FinishedAt}");
            Console.WriteLine($"Number of unfinished tasks is => [ {user.UnFinishedTasks.Count} ]");

            Console.WriteLine();
        }

        private static void ShowBorder(string test)
        {
            Console.Clear();
            AnswerConsole();
            var temp = test;
            Console.WriteLine("╔" + new string('═', temp.Length) + "╗");
            Console.WriteLine("║" + temp + "║");
            Console.WriteLine("╚" + new string('═', temp.Length) + "╝");

            Console.ResetColor();
        }

        private static async Task ShowTeams()
        {
            Console.Clear();
            var teams = await RequestService.Task4();

            ShowBorder("Teams with performers older when 10 years");

            foreach (var team in teams)
            {
                Console.WriteLine($"Team id: {team.Id} name=> ( {team.Name} ) create at {team.CreatedAt}");
                Console.WriteLine("Performers are:");
                foreach (var user in team.TeamPerformes)
                {
                    Console.WriteLine($"Perfromer ID: {user.Id} name {user.FirstName} last name {user.LastName}");
                }
                Console.WriteLine();
            }
        }

        static private void AnswerConsole()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static async Task ShowListOfTasksFinished()
        {
            Console.Clear();
            int ID;
            while (true)
            {
                Console.Write("Please enter user Id: ");
                try
                {
                    ID = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            var tasks = await RequestService.Task3(ID);

            if (tasks.Count == 0)
            {
                ShowBorder($"User with id {ID} don't have finished tasks in 2021");
            }
            ShowBorder($"List of tasks which user with ID = {ID} finished in 2021");
            foreach (var task in tasks)
            {
                Console.WriteLine($"Task id:{task.Id} Task name: {task.Name} description: {task.Description}");
            }
            Console.WriteLine();
        }

        private static async Task ShowListOfTasksForUser()
        {
            Console.Clear();
            int ID;
            while (true)
            {
                Console.Write("Please enter user Id: ");
                try
                {
                    ID = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            var tasks = await RequestService.Task2(ID);

            if (tasks.Count == 0)
            {
                ShowBorder($"User with id {ID} don't have tasks");
            }
            ShowBorder($"List of tasks for user {ID}");
            foreach (var task in tasks)
            {
                Console.WriteLine($"Task id:{task.Id} Task name: {task.Name} description: {task.Description}");
            }

            Console.WriteLine();
        }

        private static async Task ShowUserTasksPerProject()
        {
            Console.Clear();
            int ID;
            while (true)
            {
                Console.Write("Please enter user Id: ");
                try
                {
                    ID = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            string tasks = await RequestService.Task1(ID);

            ShowBorder($"User with Id = {ID} tasks:");
            tasks = tasks.Replace("}", Environment.NewLine + " }");
            tasks = tasks.Replace(",", "," + Environment.NewLine);
            Console.WriteLine(tasks);
            Console.WriteLine();

        }
    }
}
