﻿using AutoMapper;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Projects.BLL.Interfaces;
using Projects.BLL.Services;
using Projects.Common.DTOmodels;
using Projects.DAL;
using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Projects.BLL.Tests
{
    public class TaskServiceTests : IClassFixture<ServiceFixture>, IDisposable
    {

        private readonly ITaskService _tasksService;
        private readonly IUnitOfWork unitOfWork;
        private readonly ProjectDbContext context;

        readonly IUnitOfWork _fakeUnitOfWork;
        readonly IMapper _imapper;

        public TaskServiceTests(ServiceFixture fixture)
        {
            DbContextOptions<ProjectDbContext> options = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDB", new InMemoryDatabaseRoot())
                .Options;

            context = new ProjectDbContext(options);

            context.Database.EnsureCreated();

            unitOfWork = new UnitOfWork(context);


            _tasksService = new TaskService(unitOfWork, fixture.GetMapper);


            _fakeUnitOfWork = A.Fake<IUnitOfWork>();
            _imapper = A.Fake<IMapper>();

        }
        public void Dispose()
        {
            context.Database.EnsureDeleted();
        }


        [Fact]
        public void ChangeTaskToComplete_WhenValidDTO_ThenChanged()
        {
            var taskStateCreated = _tasksService.GetTasks().FirstOrDefault(t => t.State == 1);
            int idTaskState = taskStateCreated.Id;

            taskStateCreated.State = 2;
            var newTask = new TaskDTO()
            {
                Id = taskStateCreated.Id,
                Name = taskStateCreated.Name,
                Description = taskStateCreated.Description,
                CreatedAt = taskStateCreated.CreatedAt,
                FinishedAt = taskStateCreated.FinishedAt,
                PerformerId = taskStateCreated.PerformerId,
                ProjectId = taskStateCreated.ProjectId,
                State = taskStateCreated.State
            };


            _tasksService.Update(taskStateCreated.Id, newTask);

            var taskStateChanged = _tasksService.GetById(idTaskState);

            Assert.Equal(2, taskStateChanged.State);
        }

        [Fact]
        public void ChangeTaskToComplete_WhenCallServiceMethod_ThenCallDalMethod()
        {
            TaskService tasksService = new TaskService(_fakeUnitOfWork, _imapper);

            int ID = 2;
            var task = new TaskDTO()
            {

                Name = "BoringTask",
                Description = "Don't even try to solve it xD",
                CreatedAt = DateTime.Now,
                FinishedAt = DateTime.Now,
                PerformerId = 1,
                ProjectId = 1,
                State = 2
            };

            tasksService.Update(ID, task);

            A.CallTo(() => _fakeUnitOfWork.Tasks.Update(ID, A<DAL.Models.Task>._)).MustHaveHappenedOnceExactly();
        }




        [Theory]
        [InlineData(67, new int[] { 3, 263, 373, 104, 404 })]
        [InlineData(72, new int[] { 427, 386 })]
        [InlineData(69, new int[] { 142, 9, 243, 218 })]
        [InlineData(75, new int[] { 78, 12, 85, 203, 275, 405, 469 })]
        [InlineData(73, new int[] { 55, 54, 294 })]
        public void GetUnfinishedTasks_WhenValidId_ThenGetArray(int userId, int[] expectedArray)
        {
            List<TaskDTO> result = _tasksService.GetUnfinishedTasks(userId).ToList();

            int[] actualArray = result.Select(t => t.Id).ToArray();

            Array.Sort(actualArray);
            Array.Sort(expectedArray);

            // Assert

            Assert.True(actualArray.SequenceEqual(expectedArray));
        }


        [Theory]
        [InlineData(0)]
        [InlineData(-10000)]
        [InlineData(10000)]
        public void GetUnfinishedTasks_WhenNotValidId_ThenThrowArgumentException(int userId)
        {
            Assert.Throws<ArgumentException>(() => _tasksService.GetUnfinishedTasks(userId));
        }
    }
}
