﻿using AutoMapper;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Projects.BLL.Interfaces;
using Projects.BLL.Services;
using Projects.Common.DTOmodels;
using Projects.DAL;
using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Linq;
using Xunit;


namespace Projects.BLL.Tests
{
    public class UserServiceTests : IClassFixture<ServiceFixture>, IDisposable
    {
        private readonly IUserService _usersService;
        private readonly ProjectDbContext context;
        private readonly IUnitOfWork unitOfWork;

        readonly IUnitOfWork _fakeUnitOfWork;
        readonly IMapper _imapper;

        public UserServiceTests(ServiceFixture fixture)
        {
            DbContextOptions<ProjectDbContext> options = new DbContextOptionsBuilder<ProjectDbContext>().EnableSensitiveDataLogging()
                .UseInMemoryDatabase(databaseName: "TestDB", new InMemoryDatabaseRoot())
                .Options;

            context = new ProjectDbContext(options);

            context.Database.EnsureCreated();

            unitOfWork = new UnitOfWork(context);

            _usersService = new UserService(unitOfWork, fixture.GetMapper);


            _fakeUnitOfWork = A.Fake<IUnitOfWork>();
            _imapper = A.Fake<IMapper>();

        }
        public void Dispose()
        {
            context.Database.EnsureDeleted();
        }

        [Fact]
        public void CreateUser_WhenValidDTO_ThenAddUserToDB()
        {
            var countUsersBeforeCreate = _usersService.GetUsers().Count;

            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 2,
                BirthDay = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            _usersService.Create(userDTO);

            var countUsersAfterCreate = _usersService.GetUsers().Count;
            var userTest = _usersService.GetUsers().FirstOrDefault(u => u.FirstName == "FirstName" && u.LastName == "LastName");

            Assert.True(countUsersAfterCreate - countUsersBeforeCreate == 1);

            Assert.NotNull(userTest);
            Assert.Equal("FirstName", userTest.FirstName);
            Assert.Equal("LastName", userTest.LastName);
            Assert.Equal("email@mail.com", userTest.Email);
            Assert.Equal(2, userTest.TeamId);
        }

        [Fact]
        public void CreateUser_WhenCallServiceMethod_ThenCallDalMethod()
        {
            UserService usersService = new UserService(_fakeUnitOfWork, _imapper);

            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 2,
                BirthDay = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            usersService.Create(userDTO);

            A.CallTo(() => _fakeUnitOfWork.Users.Create(A<User>._)).MustHaveHappenedOnceExactly();
        }



        [Fact]
        public void CreateUser_WhenNotValidIdDTO_ThenThrowArgumentException()
        {
            var countUsersBeforeCreate = _usersService.GetUsers().Count;

            UserDTO userDTO = new UserDTO()
            {
                FirstName = "Mekola",
                LastName = "Kolian",
                Email = "tipa_mylo@mail.com",
                BirthDay = DateTime.Now.AddYears(-20),
                RegisteredAt = DateTime.Now,
                TeamId = 10000,
            };

            var countUsersAfterCreate = _usersService.GetUsers().Count;

            Assert.Throws<ArgumentException>(() => _usersService.Create(userDTO));
            Assert.True(countUsersAfterCreate - countUsersBeforeCreate == 0);
        }


        [Fact]
        public void AddUserToTeam_WhenValidDTO_ThenAdded()
        {
            var user = _usersService.GetUsers().FirstOrDefault(u => u.TeamId == null);

            int indexUser = user.Id;

            user.TeamId = 10;

            _usersService.Update(user.Id, user);

            var userChanged = _usersService.GetById(indexUser);

            Assert.Equal(10, userChanged.TeamId);

        }

        [Fact]
        public void AddUserToTeam_WhenCallServiceMethod_ThenCallDalMethod()
        {
            UserService usersService = new UserService(_fakeUnitOfWork, _imapper);

            int ID = 65;
            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 2,
                BirthDay = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            usersService.Update(ID, userDTO);

            A.CallTo(() => _fakeUnitOfWork.Users.Update(ID, A<User>._)).MustHaveHappenedOnceExactly();

        }

    }
}
