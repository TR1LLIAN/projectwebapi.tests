﻿using Projects.Common.DTOmodels;
using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Interfaces
{
    public interface IProjectService
    {
        List<ProjectDTO> GetProjects();
        void Delete(int id);
        void Create(ProjectDTO project);
        void Update(int id,ProjectDTO project);
        void Update(ProjectDTO project);
        ProjectDTO GetById(int id);
    }
}
