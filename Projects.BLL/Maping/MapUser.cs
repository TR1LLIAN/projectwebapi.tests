﻿using AutoMapper;
using Projects.Common.DTOmodels;
using Projects.DAL.Models;

namespace Projects.BLL.Maping
{
    public class MapUser : Profile
    {
        public MapUser()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>().ForMember(src => src.Id, opts => opts.Ignore());

        }
    }
}
