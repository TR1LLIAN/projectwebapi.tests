﻿using AutoMapper;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Projects.BLL.Services
{
    public class LinqService : ILinqService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public LinqService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }



        #region Aggregate all data
        private List<ProjectModel> AggregateModels()
        {
            List<Project> projects = _unitOfWork.Projects.GetList();
            List<User> users = _unitOfWork.Users.GetList();
            List<Team> teams = _unitOfWork.Teams.GetList();
            List<DAL.Models.Task> tasks = _unitOfWork.Tasks.GetList();

            List<DAL.Models.TaskModel> taskModels = tasks.Join(users,
                t => t.PerformerId,
                u => u.Id, (t, u) => new TaskModel()
                {
                    Id = t.Id,
                    Name = t.Name,
                    FinishedAt = t.FinishedAt,
                    CreatedAt = t.CreatedAt,
                    Description = t.Description,
                    PerformerId = t.PerformerId,
                    ProjectId = t.ProjectId,
                    Performer = _mapper.Map<UserDTO>(u)
                }).ToList();

            List<DAL.Models.TeamModel> teamModels = teams.GroupJoin(users,
                t => t.Id,
                u => u.TeamId, (t, u) => new TeamModel()
                {
                    Id = t.Id,
                    Name = t.Name,
                    CreatedAt = t.CreatedAt,
                    TeamPerformes = _mapper.Map<List<UserDTO>>(u.Where(u => u.TeamId == t.Id).ToList())
                }).ToList();



            var result = projects.Join(users,
                p => p.AuthorId,
                u => u.Id,
                (p, u) => new ProjectModel
                {
                    Project = p,
                    Author = u

                }).Join(teamModels,
                p => p.Project.TeamId,
                t => t.Id,
                (p, t) => new ProjectModel
                {
                    Project = p.Project,
                    Author = p.Author,
                    Team = t
                }).GroupJoin(taskModels,
                p => p.Project.Id,
                t => t.ProjectId,
                (p, t) => new ProjectModel
                {
                    Project = p.Project,
                    Team = p.Team,
                    Author = p.Author,
                    Tasks = t.Where(t => t.ProjectId == p.Project.Id)
                    .Select(x => new TaskModel()
                    {
                        Name = x.Name,
                        Id = x.Id,
                        Description = x.Description,
                        PerformerId = x.PerformerId,
                        CreatedAt = x.CreatedAt,
                        ProjectId = x.ProjectId,
                        FinishedAt = x.FinishedAt,
                        Performer = x.Performer
                    }).ToList()
                }).ToList();

            return result;
        }
        #endregion

        public Dictionary<ProjectDTO, int> Task1(int id)
        {
            List<ProjectModel> data = AggregateModels();

            return data.Where(project => project.Tasks.Any(task => task.PerformerId == id)).ToDictionary(pr => _mapper.Map<ProjectDTO>(pr.Project), value => value.Tasks.Count(task => task.PerformerId == id));
        }

        public List<DAL.Models.TaskModel> Task2(int id)
        {
            var data = AggregateModels();

            return data.SelectMany(p => p.Tasks).Where(t => t.PerformerId == id && t.Name.Length < 45).ToList();
        }

        public List<TaskModel> Task3(int id)
        {
            var data = AggregateModels();
            return data.SelectMany(project => project.Tasks).Where(task => task.PerformerId == id && task.FinishedAt?.Year == 2021).ToList();
        }

        public List<TeamModel> Task4()
        {
            var data = AggregateModels();
            return data.Where(x => x.Team.TeamPerformes.All(x => (DateTime.Now.Year - x.BirthDay?.Year) >= 10))
                .Select(p => new TeamModel
                {
                    Name = p.Team.Name,
                    Id = p.Team.Id,
                    CreatedAt = p.Team.CreatedAt,
                    TeamPerformes = p.Team.TeamPerformes.OrderByDescending(x => x.RegisteredAt).ToList()
                }).ToList();
        }

        public List<UserTasks> Task5()
        {
            var data = AggregateModels();

            return data.SelectMany(x => x.Tasks)
                .Select(x => x.Performer).Distinct().OrderBy(x => x.FirstName)
                .GroupJoin(data.SelectMany(x => x.Tasks), p => p.Id, t => t.PerformerId, (p, t) => new UserTasks
                {
                    User = _mapper.Map<UserDTO>(p),
                    Tasks = t.Where(t => t.PerformerId == p.Id).OrderByDescending(t => t.Name.Length).ToList()
                }).ToList();
        }

        public UserModel Task6(int id)
        {
            var data = AggregateModels();

            return  data.OrderByDescending(project => project.Project.CreatedAt).Where(pr => pr.Author.Id == id)
                .Select(usermodel => new UserModel
                {
                    User = _mapper.Map<UserDTO>(usermodel.Author),
                    LastProject =_mapper.Map<ProjectDTO>( usermodel.Project),
                    LongestTask = usermodel.Tasks.OrderByDescending(x => (x?.CreatedAt - x?.FinishedAt)).FirstOrDefault(),
                    UnFinishedTasks = usermodel.Tasks.Where(x => x.FinishedAt == null).ToList()
                }).FirstOrDefault();
        }

        public List<ProjectInfo> Task7()
        {
            List<ProjectModel> data = AggregateModels();
            return data.Select(x => new ProjectInfo
            {
                Project = _mapper.Map<ProjectDTO>(x.Project),
                LongestTaskDescription = x.Tasks.OrderByDescending(x => x.Description.Length).FirstOrDefault(),
                ShortestTaskByName = x.Tasks.OrderBy(x => x.Name.Length).FirstOrDefault(),
                UsersCount = ((x.Project.Name.Length > 20 || x.Tasks.Count < 3) ? x.Team.TeamPerformes.Count : null)
            }).ToList();
        }
    }
}
