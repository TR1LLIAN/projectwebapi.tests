﻿using AutoMapper;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;

namespace Projects.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(ProjectDTO project)
        {
            if (project.TeamId != null)
                _ = _unitOfWork.Teams.GetItemById((int)project.TeamId) ?? throw new ArgumentException($"Team with Id {project.TeamId} doesn't exist!");
            if (project.AuthorId != null)
                _ = _unitOfWork.Users.GetItemById((int)project.AuthorId) ?? throw new ArgumentException($"User with Id {project.AuthorId} doesn't exist!");

            _unitOfWork.Projects.Create(_mapper.Map<Project>(project));
        }

        public void Delete(int id)
        {
            _unitOfWork.Projects.Delete(id);
        }

        public ProjectDTO GetById(int id)
        {

            return _mapper.Map<ProjectDTO>(_unitOfWork.Projects.GetItemById(id));

        }

        public List<ProjectDTO> GetProjects()
        {
            return _mapper.Map<List<ProjectDTO>>(_unitOfWork.Projects.GetList());
        }

        public void Update(int id, ProjectDTO project)
        {
            _unitOfWork.Projects.Update(id, _mapper.Map<Project>(project));
        }

        public void Update(ProjectDTO project)
        {
            _unitOfWork.Projects.Update(_mapper.Map<Project>(project));
        }
    }
}
