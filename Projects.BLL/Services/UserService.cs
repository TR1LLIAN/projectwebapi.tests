﻿using AutoMapper;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;

namespace Projects.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(UserDTO user)
        {
            if (user.TeamId != null)
                _ = _unitOfWork.Teams.GetItemById((int)user.TeamId) ?? throw new ArgumentException($"Team with Id {user.TeamId} doesn't exist!");

            _unitOfWork.Users.Create(_mapper.Map<User>(user));
        }

        public void Delete(int id)
        {
            _ = _unitOfWork.Users.GetItemById(id) ?? throw new ArgumentNullException($"User with ID {id} not found!");
            _unitOfWork.Users.Delete(id);
        }

        public UserDTO GetById(int id)
        {
            return _mapper.Map<UserDTO>(_unitOfWork.Users.GetItemById(id) ?? throw new ArgumentNullException($"ID {id} not found!"));
        }

        public List<UserDTO> GetUsers()
        {
            return _mapper.Map<List<UserDTO>>(_unitOfWork.Users.GetList());
        }

        public void Update(int id,UserDTO user)
        {
            _ = _unitOfWork.Users.GetItemById(id) ?? throw new ArgumentNullException($"User with ID {id} not found!");
            _unitOfWork.Users.Update(id, _mapper.Map<User>(user));
        }

        public void Update(UserDTO user)
        {
            _unitOfWork.Users.Update(_mapper.Map<User>(user));
        }
    }
}
