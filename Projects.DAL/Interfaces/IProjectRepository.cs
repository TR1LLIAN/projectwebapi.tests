﻿using Projects.DAL.Models;


namespace Projects.DAL.Interfaces
{
    public interface IProjectRepository:IRepository<Project>
    {
    }
}
