﻿using System.Collections.Generic;


namespace Projects.DAL.Interfaces
{
    public interface IRepository<T> where T:class    
    {
        List<T> GetList();
        T GetItemById(int id);
        void Create(T item);
        void Update(int id, T item);
        void Update(T item);
        void Delete(int id); 
    }
}
