﻿using Projects.DAL.Models;


namespace Projects.DAL.Interfaces
{
    public interface IUserRepository:IRepository<User>
    {
    }
}
