﻿using Projects.Common.DTOmodels;

namespace Projects.DAL.Models
{
    public class ProjectInfo
    {
        public ProjectDTO Project { get; set; }
        public TaskModel LongestTaskDescription { get; set; }
        public TaskModel ShortestTaskByName { get; set; }
        public int? UsersCount { get; set; }

    }
}
