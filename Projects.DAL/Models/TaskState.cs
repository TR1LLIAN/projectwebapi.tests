﻿namespace Projects.DAL.Models
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}
