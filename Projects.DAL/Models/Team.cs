﻿using System;
using System.Collections.ObjectModel;

namespace Projects.DAL.Models
{
    public class Team:BaseModel
    {
        public DateTime CreatedAt { get; set; }

        public Collection<Project> Projects { get; set; }
        public Collection<User> Users { get; set; }
        
    }

}
