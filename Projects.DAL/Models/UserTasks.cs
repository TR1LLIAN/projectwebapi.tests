﻿using Projects.Common.DTOmodels;
using System.Collections.Generic;

namespace Projects.DAL.Models
{
    public class UserTasks
    {
        public UserDTO User { get; set; }
        public List<TaskModel> Tasks { get; set; }
    }
}
