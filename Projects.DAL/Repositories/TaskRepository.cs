﻿using Microsoft.EntityFrameworkCore;
using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.DAL.Repositories
{
    public class TaskRepository:Repository<Task>,ITaskRepository
    {
        public TaskRepository(ProjectDbContext tasks) : base(tasks)
        {
            
        }
    }
}
