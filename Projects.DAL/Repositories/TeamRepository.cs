﻿using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;

namespace Projects.DAL.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public TeamRepository(ProjectDbContext context) : base(context)
        {
        }
    }
}
