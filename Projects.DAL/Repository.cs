﻿using Microsoft.EntityFrameworkCore;
using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Projects.DAL
{
    public class Repository<T> : IRepository<T> where T : BaseModel
    {
        private readonly ProjectDbContext _items;

        public Repository(ProjectDbContext items)
        {
            _items = items;
        }

        public void Create(T item)
        {
            if (_items.Set<T>().Contains(item))
            {
                throw new InvalidOperationException("Such entry already exist!");
            }
            else
            {
                _items.Set<T>().Add(item);
                _items.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            T deletedItem = _items.Set<T>().FirstOrDefault(it => it.Id == id);
            if(deletedItem is null)
            {
                throw new InvalidOperationException("Cannot delete not existing item!");
            }
            _items.Set<T>().Remove(deletedItem);
            _items.SaveChanges();
        }

        public T GetItemById(int id)
        {
            return _items.Set<T>().AsNoTracking().FirstOrDefault(it => it.Id == id);
        }

        public List<T> GetList()
        {
            return _items.Set<T>().AsNoTracking().ToList();
        }

        public void Update(int id,T item)
        {
            if (_items.Set<T>().Any(x=>x.Id == id))
            {
                item.Id = id;
                _items.Set<T>().Update(item).State = EntityState.Modified;
            }
            _items.SaveChanges();
        }

        public void Update(T item)
        {
            _items.Entry(item).State = EntityState.Modified;
        }
    }
}
