﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Projects.Common.DTOmodels;
using Projects.DAL.Context;
using ProjectStructure.WebAPI.IntegrationTests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectsWebAPI.IntegrationTests
{
    public class ControllersIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        ProjectDbContext db;
        private readonly HttpClient _client;
        public ControllersIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    var descriptor = services.SingleOrDefault(
                        d => d.ServiceType ==
                            typeof(DbContextOptions<ProjectDbContext>));

                    if (descriptor != null)
                    {
                        services.Remove(descriptor);
                    }

                    services.AddDbContext<ProjectDbContext>(options => options.UseInMemoryDatabase("TestDB")); // , new InMemoryDatabaseRoot()
                    // Build the service provider.
                    var sp = services.BuildServiceProvider();
                    // Create a scope to obtain a reference to the database
                    using (var scope = sp.CreateScope())
                    {
                        var scopedServices = scope.ServiceProvider;
                        db = scopedServices.GetRequiredService<ProjectDbContext>();

                        // Ensure the database is created.
                        db.Database.EnsureDeleted();
                        db.Database.EnsureCreated();
                    }
                });
            }
            ).CreateClient();

        }
        public void Dispose()
        {
            
        }

        //Створення проекту
        [Fact]
        public void AddProject_WhenValidDTO_ThenAddedAndCodeOK()
        {
            // кількість записів перед додаванням
            var httpResponse = _client.GetAsync(@$"api/Projects").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int counProjectBeforeAdd = entity.Count;

            // Add
            ProjectDTO projectDTO = new ProjectDTO()
            {
                Name = "TestProject",
                Description = "Description",
                TeamId = 2,
                AuthorId = 1,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(13)
            };

            string jsonInString = JsonConvert.SerializeObject(projectDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = _client.PostAsync(@"api/Projects", stringContent).Result;

            // Кол-во записей после добавления
            httpResponse = _client.GetAsync(@$"api/Projects").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int counProjectAfterAdd = entity.Count;

            // Поиск добавленной записи
            var project = entity.FirstOrDefault(p => p.Name == "TestProject");


            Assert.Equal(HttpStatusCode.OK, httpResponsePost.StatusCode);
            Assert.True(counProjectAfterAdd - counProjectBeforeAdd == 1);
            Assert.NotNull(project);

        }

        [Fact]
        public void AddProject_WhenNotValidDTO_ThenCodeBadRequest()
        {
            // кількість записів перед додаванням проекту
            var httpResponse = _client.GetAsync(@$"api/Projects").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);

            // Add
            ProjectDTO projectDTO = new ProjectDTO()
            {
                Name = "Моцний проект",
                Description = "Такий моцний, що опису не треба ;)",
                AuthorId = 29999,
                TeamId = 2,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(15)
            };

            string jsonInString = JsonConvert.SerializeObject(projectDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = _client.PostAsync(@"api/Projects", stringContent).Result;

            Assert.Equal(HttpStatusCode.BadRequest, httpResponsePost.StatusCode);
        }


        //Видалення користувача
        [Fact]
        public void DeleteUser_WhenValidDTO_ThenDeletedAndCodeNoContent()
        {
            //  кількість записів перед видаленням юзера
            var httpResponse = _client.GetAsync(@$"api/Users").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = _client.DeleteAsync(@"api/Users/65").Result;

            //  кількість записів після видалення юзера
            httpResponse = _client.GetAsync(@$"api/Users").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            // Пошук видаленого запису
            var user = entity.FirstOrDefault(u => u.Id == 65);

            Assert.Equal(HttpStatusCode.NoContent, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete - countProjectAfterDelete == 1);
            Assert.Null(user);

        }


        [Fact]
        public void DeleteUser_WhenNotValidDTO_ThenCodeBadRequest()
        {
            // кількість юзерів перед видаленням
            var httpResponse = _client.GetAsync(@$"api/Users").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = _client.DeleteAsync(@"api/Users/10000").Result;

            // к-сть юзерів після видалення
            httpResponse = _client.GetAsync(@$"api/Users").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            Assert.Equal(HttpStatusCode.BadRequest, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete == countProjectAfterDelete);

        }


        //Створення команди
        [Fact]
        public void AddTeam_WhenValidDTO_ThenAddedAndCodeOK()
        {
            // к-сть команд перед додаванням
            var httpResponse = _client.GetAsync(@$"api/Teams").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<TeamDTO>>(stringResponse);
            int countTeamsBeforeAdd = entity.Count;

            TeamDTO teamDTO = new TeamDTO()
            {
                Name = "Команда А :D",
                CreatedAt = DateTime.Now
            };

            string jsonInString = JsonConvert.SerializeObject(teamDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = _client.PostAsync(@"api/Teams", stringContent).Result;

            // кількість команд після додавання
            httpResponse = _client.GetAsync(@$"api/Teams").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<TeamDTO>>(stringResponse);
            int countTeamsAfterAdd = entity.Count;

            // Пошук доданого запису
            var team = entity.FirstOrDefault(p => p.Name == "Команда А :D");


            Assert.Equal(HttpStatusCode.OK, httpResponsePost.StatusCode);
            Assert.True(countTeamsAfterAdd - countTeamsBeforeAdd == 1);
            Assert.NotNull(team);

        }

        //Видалення завдання
        [Fact]
        public void DeleteTask_WhenValidDTO_ThenDeletedAndCodeNoContent()
        {
            // Кількість тасків перед видаленням
            var httpResponse = _client.GetAsync(@$"api/Tasks").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = _client.DeleteAsync(@"api/Tasks/2").Result;

            // Кількість тасків після видаленням
            httpResponse = _client.GetAsync(@$"api/Tasks").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            // Пошук видаленої таски
            var task = entity.FirstOrDefault(u => u.Id == 2);

            Assert.Equal(HttpStatusCode.NoContent, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete - countProjectAfterDelete == 1);
            Assert.Null(task);

        }


        [Fact]
        public void DeleteTask_WhenNotValidDTO_ThenCodeBadRequest()
        {
            // кількість тасків перед видаленням
            var httpResponse = _client.GetAsync(@$"api/Tasks").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = _client.DeleteAsync(@"api/Tasks/20010").Result;

            // к-сть тасків після видалення
            httpResponse = _client.GetAsync(@$"api/Tasks").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            Assert.Equal(HttpStatusCode.BadRequest, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete == countProjectAfterDelete);

        }

        [Fact]
        public void AddUser_WhenValidDTO_ThenAddedAndCodeOK()
        {
            // к-сть юзерів перед додаванням нового
            var httpResponse = _client.GetAsync(@$"api/Users").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<UserDTO>>(stringResponse);
            int countUsersBeforeAdd = entity.Count;

            UserDTO userDTO = new UserDTO()
            {
                FirstName = "Mejson",
                LastName = "Dzizio",
                BirthDay = DateTime.Now.AddYears(-60),
                Email = "Tipa_ne_moje_mylo@mail.com",
                RegisteredAt = DateTime.Now,
                TeamId = 2,
            };

            string jsonInString = JsonConvert.SerializeObject(userDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = _client.PostAsync(@"api/Users", stringContent).Result;

            // к-сть записів після додавання юзера
            httpResponse = _client.GetAsync(@$"api/Users").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<UserDTO>>(stringResponse);
            int countUsersAfterAdd = entity.Count;

            // пошук доданого юзера
            var project = entity.FirstOrDefault(p => p.FirstName == "Mejson");


            Assert.Equal(HttpStatusCode.OK, httpResponsePost.StatusCode);
            Assert.True(countUsersAfterAdd - countUsersBeforeAdd == 1);
            Assert.NotNull(project);

        }

        [Fact]
        public async Task AddUser_WhenNotVaidDTO_ThenCodeBadRequestAsync()
        {
            // Add
            UserDTO userDTO = new UserDTO()
            {
                FirstName = "Mekola",
                LastName = "Kolian",
                Email = "tipa_mylo@mail.com",
                BirthDay = DateTime.Now.AddYears(-20),
                RegisteredAt = DateTime.Now,
                TeamId = 10000,
            };

            string jsonInString = JsonConvert.SerializeObject(userDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost =  _client.PostAsync(@"api/Users/", stringContent).Result;

            Assert.Equal(HttpStatusCode.NotFound, httpResponsePost.StatusCode);
        }


        [Theory]
        [InlineData(67, new int[] { 3, 263, 373, 104, 404 })]
        [InlineData(72, new int[] { 427, 386 })]
        [InlineData(69, new int[] { 142, 9, 243, 218 })]
        [InlineData(75, new int[] { 78, 12, 85, 203, 275, 405, 469 })]
        [InlineData(73, new int[] { 55, 54, 294 })]

        public void GetUnfinishedTasks_WhenValidId_ThenGetArray(int userId, int[] expectedArray)
        {
            // к-сть незаверершених тасків
            var httpResponse = _client.GetAsync(@$"api/Tasks/UnfinishedTasks/{userId}").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);

            int[] actualArray = entity.Select(t => t.Id).ToArray();

            Array.Sort(actualArray);
            Array.Sort(expectedArray);

            // Assert

            Assert.True(actualArray.SequenceEqual(expectedArray));
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);

        }



        [Theory]
        [InlineData(0)]
        [InlineData(10000)]
        [InlineData(-10000)]

        public void GetUnfinishedTasks_WhenNotValidId_ThenThrowArgumentException(int userId)
        {
            var httpResponse = _client.GetAsync(@$"api/Tasks/UnfinishedTasks/{userId}").Result;

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);

        }

    }
}
