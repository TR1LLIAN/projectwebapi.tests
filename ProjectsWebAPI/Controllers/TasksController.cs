﻿using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;


namespace ProjectsWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;
        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        [HttpGet]
        public ActionResult<List<TaskDTO>> Get()
        {
            return Ok(_taskService.GetTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> GetById(int id)
        {
            return Ok(_taskService.GetById(id));
        }

        [Route("{id}")]
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            try
            {
                _taskService.Delete(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPost]
        public ActionResult Post([FromBody] TaskDTO task)
        {
            try
            {
                _taskService.Create(task);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPut]
        public ActionResult Put(int id, [FromBody] TaskDTO task)
        {
            try
            {
                _taskService.Update(id, task);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpGet("UnfinishedTasks/{id}")]
        public ActionResult<TaskDTO> GetUnfinishedTasksForUser(int id)
        {
            List<TaskDTO> result;

            try
            {
                result = _taskService.GetUnfinishedTasks(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            if (result.Count == 0)
                return NotFound();

            return Ok(result);
        }
    }
}
